﻿using System.Threading.Tasks;

namespace ImageGenerator.BusinessLogic.Services.Image
{
    public interface IGenerateImageService
    {
        Task GenerateImages(string numberOfFiles);
    }
}
