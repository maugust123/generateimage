﻿using FluentValidation;

namespace ImageGenerator.BusinessLogic.Validators
{
    public class InputValidator : AbstractValidator<string>
    {
        public InputValidator()
        {
            RuleFor(input => input).NotEmpty();
            RuleFor(input => input).Must(BeAValidInteger).WithMessage("Input must be an integer");
            RuleFor(input => input).Must(BeAValidIntegerGreaterThanZero).WithMessage("Input must be an integer greater than zero");

        }

        private bool BeAValidInteger(string input)
        {
            return int.TryParse(input, out int _);
        }

        private bool BeAValidIntegerGreaterThanZero(string input)
        {
            int.TryParse(input, out int result);
            return result > 0;
        }
    }

}
