﻿using System.Collections.Generic;

namespace ImageGenerator.BusinessLogic.ValidationServices
{
    public interface IUserInputValidationService
    {
        bool ValidateUserInput(string input, List<string> errors);
    }
}
